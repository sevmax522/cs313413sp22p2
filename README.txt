﻿no TODOs done >> 6 tests passed in 143ms


1st TODO done >> 6 tests passed, appears as though the linked list has faster run time at 115ms, 116ms


2nd TODO done >> 7 tests passed after correctly adding expected values at 121ms, 110ms, 107ms


3rd TODO done >> 7 tests passed, no difference seen after attempting to remove 77


4th TODO done >> 7 tests passed in 114ms, assertion was true when seeing if list had all the elements


5th TODO done >> 8 tests passed in 126ms, calculated expected average from 7 numbers


6th TODO done >> 8 tests passed, no differences in 114ms


7th TODO >> 8 tests passed, 103ms, correct expected values added


8th TODO >> 9 tests passed, 120ms, asserted the fact that the list contains 77


9th TODO >>10 tests passed, 132 ms, correct expected values added


10th TODO >>11 tests passed, 122 ms, correct expected values added


11th TODO >>12 tests passed, 138 ms, correct expected values added


12th TODO >>13 tests passed, 116 ms, made sure it contains all the required and did not contains the numbers that should not be included


13th TODO >> 14 tests passed, 119 ms, added the necessary integers so that the assertions would pass


14th TODO >> 15 tests passed, 106 ms, removed the necessary elements so the assertions pass


15th TODO >> 16 tests passed, 108 ms, retained the 77 so that the assertions would pass


16th TODO >> 17 tests passed, 104 ms, changed elements 1, 3 and 5 to 99 so the assertions would pass


17th TODO >> 18 tests passed, 107 ms, subList edited so the assertions pass
* Found error in a boolean that expected true but received false >> 19 tests passed
 
18th TODO >> 
* Size of 10 had a run time of 104ms
* Size of 100 had a runt time of 125ms
* Size of 1000 had a run time of 537ms
* Size of 10000 had a run time of 5sec826ms
* Size of 100000 had a run time over a minute